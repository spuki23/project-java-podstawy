package A10ClassSamochod;

public class Samochod {
    String nadwozie;
    int rokProdukcji;
    short iloscDrzwi;
    String kolor;


    Samochod(){

    }
    Samochod (String nadwozie, int rokProdukcji, short iloscDrzwi, String kolor) {
        this.nadwozie = nadwozie;
        this.rokProdukcji = rokProdukcji;
        this.iloscDrzwi = iloscDrzwi;
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nadwozie='" + nadwozie + '\'' +
                ", rokProdukcji=" + rokProdukcji +
                ", iloscDrzwi=" + iloscDrzwi +
                ", kolor='" + kolor + '\'' +
                '}';
    }
    //  SET'S

    public void setNadwozie(String nadwozie) {
        this.nadwozie = nadwozie;
    }

    public void setRokProdukcji(int rokProdukcji) {
        this.rokProdukcji = rokProdukcji;
    }

    public void setIloscDrzwi(short iloscDrzwi) {
        this.iloscDrzwi = iloscDrzwi;
    }

    public void serKolor(String kolor) {
        this.kolor = kolor;
    }

//  GET'S

    public String getNadwozie() {
        return this.nadwozie;
    }
    public int getRokProdukcji() {
        return this.rokProdukcji;
    }
    public short getIloscDrzwi() {
        return getIloscDrzwi();
    }
    public String getKolor() {
        return getNadwozie();
    }

}
