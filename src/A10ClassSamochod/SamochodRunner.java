package A10ClassSamochod;

public class SamochodRunner {
    public static void main(String[] args) {
        Samochod samochod1 = new Samochod();
        samochod1.nadwozie = "sedan";
        samochod1.rokProdukcji = 2021;
        samochod1.iloscDrzwi = 4;
        samochod1.kolor = "bialy";

//        System.out.printf(samochod.nadwozie + samochod.rokProdukcji + samochod.iloscDrzwi + samochod.kolor);
        System.out.printf("%s %s %s %s", samochod1.nadwozie, samochod1.rokProdukcji, samochod1.iloscDrzwi, samochod1.kolor);
        System.out.println();
        System.out.println("----------------");

        Samochod samochod2 = new Samochod();
        samochod2.nadwozie = "hatchback";
        samochod2.rokProdukcji = 2019;
        samochod2.iloscDrzwi = 3;
        samochod2.kolor = "czarny";

        System.out.printf("%s %s %s %s", samochod2.nadwozie, samochod2.rokProdukcji, samochod2.iloscDrzwi, samochod1.kolor);
        System.out.println();
        System.out.println("----------------");

        Samochod samochod3 = new Samochod("cabriolet", 2015, (short) 2, "czerwony");
        System.out.println(samochod3.toString());



    }
}
