import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

import static java.util.Arrays.*;

public class A07Arrays {
    public static void main(String[] args) {
        String[] array = new String[]{"Hello", "World", "!"};

        int[] myNumbers = new int[5];
        int[] myNumbers2 = new int[]{1, 2, 3};

        long[] myLongs = new long[3];

//        raz zadeklarowana tablica nie zmienia swojego typu
//        tablica moze przechowywac dane tylko jednego typu

//      zawiera 4 puste elementy NULL
//        String[] imiona = new String[4];
//        imiona[0] = "Jan";
//        imiona[3] = "roman";
//
//        System.out.println("Element numer 1: '" + imiona[0]);
//        System.out.println("Element numer 2: '" + imiona[1]);
//        System.out.println("Element numer 3: '" + imiona[2]);
//        System.out.println("Element numer 4: '" + imiona[3]);
//
//        int tabLenght = 4;
//        String[] imiona2 = new String[tabLenght];
//        imiona2[0] = "Jan";
//        imiona2[3] = "roman";
//
//        for (int i = 0; i < tabLenght; i++) {
//            System.out.println("Element numer " + i + ": " + imiona2[i]);
//        }

        int[] arrayA = new int[5];
        arrayA[0] = 400;
        arrayA[1] = 34;
        arrayA[2] = 76;
        arrayA[3] = 29;
        arrayA[4] = 59;

        int[] arrayB = new int[]{6, 5, 22, 36, 555};


        for (int i = 0; i < arrayA.length; i++) {
            System.out.print(arrayA[i] + ", ");
        }
        System.out.println("");
        System.out.println("----------");


        for (int i = 0; i < arrayB.length; i++) {
            System.out.print(arrayB[i] + ", ");
        }
        System.out.println("");
        System.out.println("----------");


        for (int i = 0; i < arrayA.length; i++) {
            System.out.print(arrayA[i] + arrayB[i] + ", ");
        }
        System.out.println("");
        System.out.println("----------");


        for (int part : arrayA) {
            System.out.print(part + ", ");
        }
        System.out.println("");
        System.out.println("----------");


        int[][] arrA = new int[3][3];
        arrA[0][0] = 15;
        arrA[0][1] = 25;
        arrA[0][2] = 15;
        arrA[1][0] = 88;
        arrA[1][1] = 15;
        arrA[1][2] = 1554;
        arrA[2][0] = 134;
        arrA[2][1] = 98;
        arrA[2][2] = 19;

        for (int i = 0; i < arrA.length; i++) {
            for (int j = 0; j < arrA.length; j++) {
                System.out.print(arrA[i][j] + ", ");
            }
            System.out.println("");
        }
        System.out.println("");
        System.out.println("----------");


        int[][] liczby;
        liczby = new int[2][2];
        for (int[] row : liczby) Arrays.fill(row, 560);
        for (int i = 0; i < liczby.length; i++) {
            for (int j = 0; j < liczby.length; j++) {
                System.out.print(liczby[i][j] + ", ");
            }
            System.out.println("");
        }


//        wczytywanie od Pawla
//        System.out.println("ze skanerem");
//        System.out.println("podaj rozmiar tab:");
//        Scanner scan = new Scanner(System.in).useLocale(Locale.US);
//        int rozTab3 = Integer.parseInt(scan.nextLine());
//        System.out.println("rozmiar tab to: "+ rozTab3);
////        for (int i=0; i< rozTab3) {
////
////        }

        int variable1 = 0;
        int[] tablicaA = {1, 2, 3, 4, 5, 6, 7};
        for (int j : tablicaA) {
            variable1 = variable1 + j;
        }
        System.out.println("suma liczb z tablicy to: " + variable1);

        //for (typZmiennychWZbiorze nazwaElementuWKonkretnejIteracji : zbiorKtoryPrzegladasz) {
        //  tresc;
        //}



        int[][] tabliczka = new int[10][10];
        for (int i =1; i<=10; i++) {
            for (int j=1; j<= 10; j++) {
                System.out.print(String.format("%4s", i*j));
            }
            System.out.println();
        }



    }

}
