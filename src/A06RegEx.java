import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class A06RegEx {
    public static void main(String[] args) {


        //        Pattern pattern = Pattern.compile(".*w3schools", Pattern.CASE_INSENSITIVE);
//        .* oznacza, ze jest cos przed lub po wyrazeniu

        //[A-Z] - grupa znakow wielkich liter
        //[a-z] - grupa znakow malych liter

        // \\s - spacja
        // \w -  dowolna ilosc znakow i liczb
//.*\@.*\.pl

        // \\w+ [@]  \\w+ \\. [a-z]{2,3}
        // słowo + @ + slowo + . + 2-3 znaki

        // \\w+ [@]  \\w+ \\. [a-z]{2,7}


//      sprawdzenie adresu EMAIL
//        Pattern pattern = Pattern.compile("\\w+[@]\\w+\\.[a-z]{2,3}", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher("moja.adres@wp.pl");
//        boolean matchFound = matcher.find();
//        if (matchFound) {
//            System.out.println("Match found");
//        } else {
//            System.out.println("Match not found");
//        }

//        sprawdzenie adresu
//        \w+[@]\w+\.[a-z]{2,3}

//        grupowanie stringow
//        [^-]*--(\\w+)(--).*

        //nazwa grupy definiujemy przez
        // ?<groupName>

        Pattern pattern = Pattern.compile("[A-Za-z\\s]+(?<kwotaKedytu>\\d+)[a-z\\s]+(?<odsetki>\\d+).*", Pattern.CASE_INSENSITIVE);

//        Scanner scan  = new Scanner(System.in);
//        System.out.println("Nazwa Twojej ulicy");
//        String textLine = scan.nextLine();

        Matcher matcher = pattern.matcher("Mam kredyt 3000 zl odsetki to 200 zl");
        boolean matchFound = matcher.matches();
        if (matchFound) {
            System.out.println("Match found");
            System.out.println(matcher.group(0));
            System.out.println(matcher.group("kwotaKredytu"));
            System.out.println(matcher.group("odsetki"));

        } else {
            System.out.println("Match not found");
        }

        //
        // ul.\.\s[A-Za-z0-9]+\s\w*\s\d+/\d+

    }
}
