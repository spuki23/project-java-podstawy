package A11Okrag;

import java.text.DecimalFormat;

public class Okrag {


//    NON STATIC

    private Double promien;

    public Okrag(Double promien) {
        this.promien = promien;
    }

    public Double getObwod() {
        Double obwod = 2* Math.PI*this.promien;
        return obwod;
    }

    public Double getPole() {
        Double pole = Math.pow(this.promien, 2.0);
        return promien;
    }



//    STATIC
    public static double radiusCounting(double radius) {
//        2x pi x r
        Double circuit = 2 * Math.PI * radius;
        return circuit;
    }

    public static double fieldCounting(double radius) {
//        pi x r ^ 2
        Double field = Math.PI * Math.pow(radius, 2);
//        DecimalFormat df = new DecimalFormat("###,###");
//        String dx = df.format(field);
//        return Double.valueOf(dx);
        return Math.round(field);


    }


//    double x=123.45678;
//DecimalFormat df = new DecimalFormat("#.##");
//String dx=df.format(x);
//x=Double.valueOf(dx);
}
