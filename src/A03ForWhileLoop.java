public class A03ForWhileLoop {
    public static void main(String[] args) {


//        int variableA = 100;
//        for (int i = 1; i <= variableA; i++) {
//            if (i % 2 == 0) {
//                System.out.println(i + ". liczba parzysta");
//            } else {
//                System.out.println(i + ". liczba nieparzysta");
//            }
//        }


//        int a = 15;
//        int b = 5;
//        int c = a%b;
//        System.out.println(c);


//        System.out.println("");


//        String[] arrayA = {"Ala", "ma", "kota"};
//
//        for (int i=0; i<3; i++) {
//            System.out.print(arrayA[i] + " ");
//        }

//        int i = 0;
//        while (i < 100) {
//            if (i % 2 == 0) {
//                System.out.println(i + ". liczba parzysta");
//            } else {
//                System.out.println(i + ". liczba nieparzysta");
//            }
//            ++i;
//        }

//        int h = 10;
//        do {
//            System.out.println("Hello World!");
//            ++h;
//        } while (h<10);
//
//
//        for (int j = 1; j < 100; j++) {
//            System.out.println(j + ". Aaaaaa");
//            if (j == 33) {
//                break;
//            }
//        }

//
//
//
//        int k = 0;
//        do {
//            System.out.println(k + ". Aaaaaa");
//            if (k == 33) {
//                break;
//            }
//            k++;
//        }
//        while (k <= 100) ;


        for (int i=0; i<10; i++) {
            if (i==8) {
                continue;
            }
            System.out.println(i+ ". Hello Trolle :D");
        }
    }
}

