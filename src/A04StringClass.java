import java.util.Locale;

public class A04StringClass {
    public static void main(String[] args) {
        //dodawanie i drukowanie tekstu
        String text = "This is ";
        text += "my text";
        System.out.println(text);



//        dodawanie stringów i porownanie
        String text1 = "This is a test";
        String text2 = "This is a test";
        String text3 = new String("This is a test");

        String val1 = text1.intern();
        String val2 = text2.intern();

        System.out.println(text1.equals(text2));
        System.out.println(text2.equals(text3));
        System.out.println(text1.equals(text3));

        System.out.println("");

        System.out.println(text1 == text2);
        System.out.println(text2 == text3);
        System.out.println(text1 == text3);

        //dodawanie Stringów
        String text12 = "My name is ";
        String text13 = "John Doe";
        String finalText = text12 + text13;
        System.out.println(finalText);

        String text31 = "This is ";
        String text41 = "a test";
        String finalText2 = text31.concat(text41);
        System.out.println(finalText2);



        //porównanie wartości Stringów
        System.out.println("This is test value".length());
        String testValue = "This is test value";
        System.out.println((testValue.toUpperCase()));
        System.out.println(testValue.toLowerCase());


        //metoda zwracajaca numer od kiedy zaczyna sie wyraz
        String testValueB = "This is test value";
        System.out.println(testValueB.indexOf("value"));


        //klasa zamieniająca litery
        String textA = ("Hahahahah! Funny joke!");
        System.out.println(textA.replaceAll("Haha", "L"));






    }

}
