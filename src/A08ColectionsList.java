import java.util.ArrayList;
import java.util.List;

public class A08ColectionsList {
    public static void main(String[] args) {
        List<String> mojaLista = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            mojaLista.add("Element " + i);
        }


        for (String elementListy : mojaLista) {
            System.out.println(elementListy);
        }
        //probuje wywolac z wartoscia 5 a nie piaty element
        mojaLista.remove(2);

//        mojaLista.remove();

//        for (String elementListy : mojaLista) {
//            System.out.println(elementListy);
//        }




//        usuwa z listy
//        mojaLista.remove("Element 1");


    }
}
