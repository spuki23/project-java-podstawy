import java.util.Locale;
import java.util.Scanner;

public class A05WczytywanieStringow {
    public static void main(String[] args) {

//      skaner wczytuje ale potrzbne mu podanie lokalizacji US

//        nie czyta znakunowej linii podczas naciskania Entera dlatego wyw

        Scanner scan = new Scanner(System.in).useLocale(Locale.US);


        System.out.println("Wpisz swoja liczbe:");
        int userNumber = Integer.parseInt(scan.nextLine());
        System.out.println(userNumber);

        System.out.println("Wpisz swoj tekst:");
        String textLine = scan.nextLine();
        System.out.println(textLine);












        int wiersze = Integer.parseInt(scan.nextLine());














//        System.out.println("Wpisz swoja liczbe:");
//        int userNumber = scan.nextInt();
//        System.out.println(userNumber);


        //drukowanie tekstu ze zmiennymi
//              *e –liczba zmiennoprzecinkowa w notacji wykładniczej
//              * f –liczba zmiennoprzecinkowa
//              *x –liczba całkowita w szesnastkowym systemie liczbowym (hexadycymalnym)
//              * o –liczba całkowita w ósemkowym systemie liczbowym (oktalnym)
//              * s –łańcuch znaków
//              *c –znak
//              *b –wartość logiczna


        int x = 20;
        float y = 5.5f;
        char c = 'J';
        String str = "Hello Java";
        //Displaying formatted string
        System.out.printf("The formatted string: %d %f %c %s", x, y, c, str);

//      drukowanie tekstu print i dodanie linii przy pomocy \n

        System.out.print("\nAla\n");
        System.out.print("ma kota\n");


    }
}
