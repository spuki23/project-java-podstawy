package A10ClassCzlowiek;

public class CzlowiekRunner {
    public static void main(String[] args) {







//        System.out.println(czlowiekImie);
        Czlowiek czlowiek = new Czlowiek();
        czlowiek.setImie("Mariusz");
        czlowiek.setNazwisko("Charusta");
        czlowiek.setWiek((short) 18);
        czlowiek.setWzrost(178);

        String czlowiekImie = czlowiek.getImie();
        String czlowiekNazwisko = czlowiek.getNazwisko();
        int czlowiekWzrost = czlowiek.getWzrost();
        short czlowiekWiek = czlowiek.getWiek();
        System.out.printf("%s %s %s %s",czlowiekImie, czlowiekNazwisko, czlowiekWiek, czlowiekWzrost);

        System.out.println("");
        System.out.println("---------------");




        Czlowiek czlowiek2 = new Czlowiek();
        czlowiek2.setImie("Dariusz");
        czlowiek2.setNazwisko("Kowalski");
        czlowiek2.setWiek((short) 30);
        czlowiek2.setWzrost(220);

        String czlowiek2Imie = czlowiek2.getImie();
        String czlowiek2Nazwisko = czlowiek2.getNazwisko();
        int czlowiek2Wzrost = czlowiek2.getWzrost();
        short czlowiek2Wiek = czlowiek2.getWiek();

        System.out.printf("%s %s %s %s", czlowiek2Imie, czlowiek2Nazwisko, czlowiek2Wiek, czlowiek2Wzrost);


        Czlowiek czlowiek3 = new Czlowiek("Maria", "Konopnicka", 160, (short) 45);
        System.out.println(czlowiek3.toString());

        Czlowiek czlowiek4 = new Czlowiek("Tadeusz", "Kosciuszko", 180);
        System.out.println(czlowiek4.toString());

        Czlowiek czlowiek5 = new Czlowiek();
        czlowiek5.setImie("Janek");
        System.out.println(czlowiek5.toString());










    }
}
