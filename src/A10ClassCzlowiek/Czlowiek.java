package A10ClassCzlowiek;

/*
klasy tworzymy za pomoca kontruktorów
za pomocą klas tworzymy obiekty

*/

// PUBLIC
// PRIVATE
// PROTECTED - powoduje, ze pewne rzeczy mozemy wykonywac tylko w ramach pakietu

//PAKIETY
// - elementy projektu
// nazwa odwróconej domeny




public class Czlowiek {
    private String imie;
    private String nazwisko;
    private int wzrost;
    private short wiek;


    public Czlowiek() {
    }
    public Czlowiek(String imie, String nazwisko, int wzrost) {
        this.imie = imie;
        this.nazwisko=nazwisko;
        this.wzrost = wzrost;
        this.wiek = wiek;
    }

    public Czlowiek(String imie, String nazwisko, int wzrost, short wiek) {
        this.imie = imie;
        this.nazwisko=nazwisko;
        this.wzrost = wzrost;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Czlowiek{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wzrost=" + wzrost +
                ", wiek=" + wiek +
                '}';
    }

    void setImie(String imie) {
        this.imie = imie;
    }

    void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }

    void setWiek(short wiek) {
        this.wiek = wiek;
    }

    String getImie() {
        return this.imie;
    }

    String getNazwisko() {
        return this.nazwisko;
    }

    int getWzrost() {
        return this.wzrost;
    }

    short getWiek() {
        return this.wiek;
    }


}
